﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocatorsNUnit
{
    public static class Locators
    {
        private static Dictionary<string, By> headerElements = new Dictionary<string, By>()
        {
            { "CrimeAndJustice", By.CssSelector("div.header__left > nav > div > div:nth-child(1) > a") },
            { "EnergyAndEnvironment", By.CssSelector("div.header__left > nav > div > div:nth-child(2) > a") },
            { "ExtremeWeather", By.CssSelector("div.header__left > nav > div > div:nth-child(3) > a") },
            { "More", By.CssSelector("div.header__left > nav > div > div:nth-child(5) > a") },
            { "TermsOfUse", By.CssSelector("#pageFooter nav > a:nth-child(1)") },
            { "HeaderSearchButton", By.CssSelector("#headerSearchIcon") },
            { "HeaderMenuButton", By.Id("headerMenuIcon") },
        };

        private static Dictionary<string, By> searchPanelElements = new Dictionary<string, By>()
        {
            { "SearchInput", By.XPath("//*[@id=\"pageHeader\"]//input") },
            { "CloseButton", By.XPath("//*[@id=\"headerCloseIcon\"]") },
            { "World", By.XPath("//*[@id=\"pageHeader\"]//nav/ul/li[1]/a") },
            { "USPolitics", By.XPath("//*[@id=\"pageHeader\"]//nav/ul/li[2]/a") },
            { "Business", By.XPath("//*[@id=\"pageHeader\"]//nav/ul/li[3]/a") },
        };

        private static Dictionary<string, By> africaItemWithDifferentAxis = new Dictionary<string, By>()
        {
            { "FullPath", By.XPath("//*[@id=\"pageHeader\"]/div/div/div[2]/div/nav[2]/ul/li[1]/ul/li[1]") },
            { "ChildAxis", By.XPath("//nav[2]/ul/li[1]/ul/li[1]") },
            { "DescendantAxis", By.XPath("//nav[2]//li[1]//li[1]") },
            { "ParentAxis", By.XPath("//nav[2]//li[1]//li[1]/a/..") },
            { "AncestorAxis", By.XPath("//nav[2]/ul/li[1]/ul/li[1]/a//ancestor::li[1]") },
        };

        public static By HeaderLogo { get { return By.CssSelector("header a.brand-logo__logo-link"); } }
        public static By TermsOfUse { get { return By.XPath("//*[@id=\"maincontent\"]"); } }
        public static Dictionary<string, By> Header { get { return headerElements; } }
        public static Dictionary<string, By> SearchPanel { get { return searchPanelElements; } }
        public static Dictionary<string, By> AfricaAxis { get { return africaItemWithDifferentAxis; } }
    }
}
