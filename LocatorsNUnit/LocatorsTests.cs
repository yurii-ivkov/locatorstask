using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace LocatorsNUnit
{
    public class Tests
    {
        IWebDriver driver;
        WebDriverWait wait;
        const string url = "https://edition.cnn.com/us";

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        }

        [TestCase("CrimeAndJustice")]
        [TestCase("EnergyAndEnvironment")]
        [TestCase("ExtremeWeather")]
        [TestCase("TermsOfUse")]
        [TestCase("HeaderSearchButton")]
        [TestCase("HeaderMenuButton")]
        public void HeaderElementsFound(string locatorKey)
        {
            driver.Url = url;

            var element = wait.Until(d => d.FindElement(Locators.Header[locatorKey]));

            Assert.IsTrue(element.Displayed);
            driver.Quit();
        }

        [TestCase("SearchInput")]
        [TestCase("CloseButton")]
        [TestCase("World")]
        [TestCase("USPolitics")]
        [TestCase("Business")]
        public void SearchPanelElementsFound(string locatorKey)
        {
            driver.Url = url;
            var element = wait.Until(d => d.FindElement(Locators.Header["HeaderSearchButton"]));

            element.Click();
            element = wait.Until(d => d.FindElement(Locators.SearchPanel[locatorKey]));

            Assert.IsTrue(element.Displayed);
            driver.Quit();
        }

        [TestCase("FullPath", "li")]
        [TestCase("ChildAxis", "li")]
        [TestCase("DescendantAxis", "li")]
        [TestCase("ParentAxis", "li")]
        [TestCase("AncestorAxis", "li")]
        public void SearchSameElementWithDifferentAxis(string locatorKey, string expectedTag)
        {
            driver.Url = url;
            var element = wait.Until(d => d.FindElement(Locators.Header["HeaderSearchButton"]));

            element.Click();
            element = wait.Until(d => d.FindElement(Locators.AfricaAxis[locatorKey]));

            bool result = element.Displayed && element.TagName == "li";

            Assert.IsTrue(result);
            driver.Quit();
        }

        [Test]
        public void TermsOfUseHeaderFound()
        {
            driver.Url = url;
            var element = wait.Until(d => d.FindElement(Locators.Header["TermsOfUse"]));

            element.Click();
            element = wait.Until(d => d.FindElement(Locators.TermsOfUse));

            Assert.IsTrue(element.Displayed);
            driver.Quit();
        }
    }
}